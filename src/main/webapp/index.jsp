<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Job Finder</title>
	<style>
		div{
			margin: 5px 0;
		}
		#container{
			width: 30%;
		}
	</style>
</head>
<body>
	<div>
		<h1>Welcome to Servlet Job Finder!</h1>
		<form action="register" method="post">
			<div>
				<label for="fname"> First Name</label>
				<input type="text" name="fname" required>
			</div>
			<div>
				<label for="lname">Last Name</label>
				<input type="text" name="lname" required>
			</div>
			<div>
				<label for="phone">Phone</label>
				<input type="tel" name="phone" required>
			</div>
			<div>
				<label for="email">Email</label>
				<input type="email" name="email" required>
			</div>
			
			<fieldset  id="container">
				<legend>How did you discover the app?</legend>
				<div>
				<input type="radio" id="friends" name="discovery" value="friends" required>
				<label for="friends">Friends</label>
				</div>
				<div>
				<input type="radio" id="socialmedia" name="discovery" value="social media" required>
				<label for="socialmedia">Social Media</label>
				</div>
				<div>
				<input type="radio" id="others" name="discovery" value="others" required>
				<label for="others">Others</label>
				</div>
			</fieldset>
			<div>
				<label for="birthdate">Date of Birth</label>
				<input type="date" name="birthdate" required>
			</div>
			<div>
				<label for="usertype">Are you an Employer or Applicant?</label>
				<select id="usertype" name="usertype" required>
					<option value="" selected="selected">Select One</option>
					<option value="employer">Employer</option>
					<option value="applicant">Applicant</option>
				</select>
			</div>
			<div>
				<label for="profiledesc">Profile Description</label>
				<textarea name="profiledesc" maxlength="500"></textarea>
			</div>
			
			<button>Register</button>
		</form>
	</div>
</body>
</html>
